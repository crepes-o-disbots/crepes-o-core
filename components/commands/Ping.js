/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { Component, parseNameToLower } = require('../../class/Component');
const { baseEmbed } = require('../../modules/EmbedResponses');
const { SlashCommandBuilder, GatewayIntentBits, ActionRowBuilder } = require('discord.js');
const pingAgain = require('../buttons/PingAgain');
const Translator = require('../../class/Translation');


const name = parseNameToLower(__filename);
const localizedDescriptions = Translator.getAllTranslations('coreCmd.pingDescription');
const description = localizedDescriptions['en-US'];


const cmdBuilder = new SlashCommandBuilder()
    .setName(name)
    .setDescription(description)
    .setDMPermission(false);


async function ping(interaction) {
    // The bot answers to a user in an ephemeral way, use their locale as reference
    const lang = interaction.locale;

    const embed = baseEmbed(interaction.client)
        .setTitle(Translator.getString('coreCmd.pingEmbedTitle', {}, lang))
        .setDescription(Translator.getString('coreCmd.pingEmbedText', { 'ping' : interaction.client.ws.ping }, lang));

    // The button needs its label
    const builder = pingAgain.builder;
    builder.setLabel(Translator.getString('coreCmd.pingBtnLabel', {}, lang));

    await interaction.reply({
        embeds: [embed],
        components: [new ActionRowBuilder().addComponents(builder)],
        ephemeral: true,
    });
}

const command = new Component({
    name: name,
    description: description,
    disabled: false,
    category: 'Utils',
    intents: [GatewayIntentBits.Guilds],
    builder: cmdBuilder,
    mainFunction: ping,
    cooldown: 5,
});

module.exports = command;