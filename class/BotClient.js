/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { Client, ApplicationCommand, Collection } = require('discord.js');
const path = require('path');
const fs = require('fs');
const { logger } = require('./Logger');
const { baseEmbed } = require('../modules/EmbedResponses');
const Translator = require('./Translation');

/**
 * Extends the client from discord.js to add useful functions
 */
class BotClient extends Client {
    /**
     * Default constructor, doesn't need to be modified
     * @param options
     */
    constructor(options) {
        super(options);

        // Add cooldown collection allowing commands to be on cooldown
        this.cooldowns = new Collection();
    }

    /**
     * Register all events
     */
    registerEvents() {
        // Get event folder
        const eventsPath = path.join(__dirname, '../events');

        // Folder must exist
        if (!fs.existsSync(eventsPath)) {
            logger.warn(Translator.getString('core.eventNoEvents'));
            return;
        }

        // Get files
        const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));

        // Register every single one of them
        logger.debug(Translator.getString('core.eventReadPath', { 'eventPath' : eventsPath }));
        for (const file of eventFiles) {
            logger.debug(Translator.getString('core.eventReadFile', { 'file' : file }));
            const filePath = path.join(eventsPath, file);
            const event = require(filePath);
            if (event.once) {
                this.once(event.name, (...args) => event.execute(...args));
            }
            else {
                this.on(event.name, (...args) => event.execute(...args));
            }
        }
    }

    /**
     * Sets the bot's status.
     * Expects the value inside the STATUS environment variable
     */
    setStatus(status) {
        if (status !== undefined) {
            this.user.setStatus(status);
        }
    }

    /**
     * Set's up the bot's rich presence
     *
     * @param name
     * Activity name
     * @param type
     * Activity type
     * @param url
     * Activity URL
     */
    setRP(name, type, url) {
        if (url !== undefined) {
            this.user.setActivity(name, { type : type, url : url });
        }
        else if (type !== undefined) {
            this.user.setActivity(name, { type : type });
        }
        else if (name !== undefined) {
            this.user.setActivity(name);
        }
    }

    /**
     * Updates commands using currently stored ones. Doesn't support guild commands.
     * @param commands
     * Array of commands, provided in JSON format
     */
    updateCommands(commands) {
        logger.info(Translator.getString('core.cmdApiUpdate'));
        this.application.commands.fetch()
            .then(async currentCommands => {
                logger.debug(Translator.getString('core.cmdApiNbDetected', { 'cmdNb' : currentCommands.size }));

                // Add new commands
                logger.debug(Translator.getString('core.cmdApiCmdAdding'));
                const newCmds = commands.filter((cmd) => !currentCommands.some((currCmd) => cmd.name === currCmd.name));

                // Loop on added cmds
                for (const newCommand of newCmds) {
                    try {
                        await this.application.commands.create(newCommand);
                        logger.debug(Translator.getString('core.cmdApiCmdAdded', { 'cmdName' : newCommand.name }));
                    }
                    catch (err) {
                        logger.error(Translator.getString('core.cmdApiCmdAddError', { 'cmdName' : newCommand.name }));
                        logger.error(err);
                    }
                }

                // Remove missing commands
                logger.debug(Translator.getString('core.cmdApiCmdDeleting'));
                const deletedCmds = currentCommands.filter((currCmd) => !commands.some((cmd) => cmd.name === currCmd.name)).toJSON();

                // Loop on all deleted cmd
                for (const deletedCommand of deletedCmds) {
                    try {
                        await this.application.commands.delete(deletedCommand);
                        logger.debug(Translator.getString('core.cmdApiCmdDeleted', { 'cmdName' : deletedCommand.name }));
                    }
                    catch (err) {
                        logger.error(Translator.getString('core.cmdApiCmdDeleteError', { 'cmdName' : deletedCommand.name }));
                        logger.error(err);
                    }
                }

                // Update commands
                logger.debug(Translator.getString('core.cmdApiCmdUpdating'));
                const potentialUpdatedCmds = commands.filter((cmd) => currentCommands.some((currCmd) => cmd.name === currCmd.name));

                // Loop to find updated cmds
                for (const updatedCmd of potentialUpdatedCmds) {
                    const prevCmd = currentCommands.find((cmd) => cmd.name === updatedCmd.name);

                    // Command description modified, or different options
                    if (prevCmd.description !== updatedCmd.description || !ApplicationCommand.optionsEqual(prevCmd.options ?? [], updatedCmd.options ?? [])) {

                        // Name cannot be identical to the old command
                        const name = updatedCmd.name;
                        delete updatedCmd.name;

                        try {
                            await prevCmd.edit(updatedCmd);
                            logger.debug(Translator.getString('core.cmdApiCmdUpdated', { 'cmdName' : name }));
                        }
                        catch (err) {
                            logger.error(Translator.getString('core.cmdApiCmdUpdateError', { 'cmdName' : name }));
                            logger.error(err);
                        }
                    }
                }

                logger.info(Translator.getString('core.cmdApiOk'));
            })
            .catch(err => logger.error(err));
    }

    /**
     * Checks the interaction for potential cooldown
     * @param interaction
     * Interaction sent by discord
     * @param name
     * Component name
     * @param cooldown
     * Cooldown, time to wait in seconds
     * @param userId
     * Discord user id
     * @returns {boolean}
     */
    checkCooldown(interaction, { name, cooldown }, userId) {
        if (cooldown <= 0) {
            return true;
        }

        if (!this.cooldowns.has(name)) {
            this.cooldowns.set(name, new Collection());
        }

        // Get current time, get command timestamps for users, get cooldown in seconds
        const now = Date.now();
        const timestamps = this.cooldowns.get(name);
        const cooldownAmount = cooldown * 1000;

        if (timestamps.has(userId)) {
            const expiring = timestamps.get(userId) + cooldownAmount;

            if (now < expiring) {
                interaction.reply({
                    embeds: [baseEmbed(this).setTitle(Translator.getString('coreEvent.cooldownEmbedTitle')).setDescription(Translator.getString('coreEvent.cooldownEmbedText', { 'cooldownTime' : Math.round((expiring - now) / 1000) }))],
                    ephemeral: true,
                });
                return false;
            }
        }

        // Set new timeout, mark for deletion when timeout is finished
        timestamps.set(userId, now);
        setTimeout(() => timestamps.delete(userId), cooldownAmount);

        return true;
    }
}

module.exports = BotClient;