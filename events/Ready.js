/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { logger } = require('../class/Logger');
const { clearStatusAndRPData } = require('../modules/BotConf');
const { getCommandsBuilders } = require('../modules/InteractionComponents');
const Translator = require('../class/Translation');

// Export ready event
module.exports = {
    name: 'ready',
    once: true, // Execute code once
    execute(client) {

        // Status & Rich presence
        logger.info(Translator.getString('coreEvent.readySetup'));
        client.setStatus(process.env.STATUS);
        client.setRP(process.env.ACTIVITY_NAME, process.env.ACTIVITY_TYPE, process.env.ACTIVITY_URL);
        clearStatusAndRPData();

        // Update commands
        client.updateCommands(getCommandsBuilders());

        logger.info(Translator.getString('coreEvent.readyOk', { 'user' : client.user.tag }));
    },
};