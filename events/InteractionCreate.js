/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { logger } = require('../class/Logger');
const { getCommands, getButtons } = require('../modules/InteractionComponents');
const { errorEmbed } = require('../modules/EmbedResponses');
const Translator = require('../class/Translation');

/**
 * Executes code to send how the command can be autocompleted
 * @param interaction
 * The command interaction
 * @param command
 * Command used by the user
 */
async function autocompleteInteraction(interaction, command) {
    // The function exists, since it has already been checked with loadCommands
    await command.autocompleteFunction(interaction);
}

/**
 * Executes code accordingly to a created interaction
 *
 * @param interaction
 * The interaction
 *
 * @param component
 * Command used by the user
 */
async function componentInteraction(interaction, component) {
    // Show command usage
    const name = (interaction.isChatInputCommand() ? 'cmd.' : 'btn.') + component.name;
    logger.info(Translator.getString('coreEvent.cmpExec', { 'user': interaction.user.tag, 'userId': interaction.user.id, 'cmpName' : name }));

    // Execute code, if function is defined
    if (!component.mainFunction) {
        await interaction.reply({
            embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.cmdLaunchErrorEmbed'))],
            ephemeral: true,
        });
        logger.error(Translator.getString('coreEvent.cmpExecError', { 'cmpName' : name }));
    }
    else {
        try {
            await component.mainFunction(interaction);
        }
        catch (err) {
            await interaction[interaction.deferred ? 'editReply' : 'reply']({
                embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.cmpExecErrorEmbed'))],
                ephemeral: true,
            });
            logger.error(err);
        }
    }
}


module.exports = {
    name: 'interactionCreate',
    async execute(interaction) {

        // Block bots and system user (a.k.a discord system)
        if (interaction.user.bot || interaction.user.system) return;

        // Block anything other than input commands or buttons since this is what is supported for now
        if (!interaction.isCommand() && !interaction.isButton()) return;

        // Get asked component
        const component = interaction.isCommand() ? getCommands().get(interaction.commandName) : getButtons().get(interaction.customId);

        // Not found
        if (!component) return;


        // Autocomplete command
        if (interaction.isAutocomplete()) {
            await autocompleteInteraction(interaction, component);
            return;
        }

        // Interaction with a component
        if (interaction.client.checkCooldown(interaction, component, interaction.user.id)) {
            await componentInteraction(interaction, component);
        }
    },
};
